# Sparse Separable Nonnegative Matrix Factorization

This repository contains the Matlab code and test scripts for Sparse Separable Nonnegative Matrix Factorization (SSNMF).
It is associated with the article [Sparse Separable Nonnegative Matrix Factorization](http://nicolasnadisic.xyz/publication/ssnmf/).
If this project is of any use in your research, please cite the article.
The version of the code we used for the experiments of our paper corresponds to [this commit](https://gitlab.com/nnadisic/ssnmf/-/tree/submitted-paper).

## Algorithm BRASSENS

The name stands for BRASSENS Relies on Assumptions of Separability and Sparsity for Elegant NMF Solving. This algorithm solves the following problem: given a matrix M, find W = M(:,J) and H column-wise k-sparse such that M ~= W * H.

This algorithms uses `arborescent`, an exact sparse nonnegative least square solver, implemented in C with a Mex interface. You have to [download it from its repository](https://gitlab.com/nnadisic/sparse-nmf), compile it, and put the Mex file in Matlab's path in order to use BRASSENS.

It also uses modified versions of [SNPA](https://epubs.siam.org/doi/10.1137/130946782), an algorithm for separable NMF designed and [implemented in Matlab](https://sites.google.com/site/nicolasgillis/code) by Nicolas Gillis.

`brassens.m` is the main function, and the one to call to solve SSNMF. Instructions are provided inside the file. It makes use of the following subroutines:
- `deltaSNPA.m` is a modified version of SNPA with a different stopping criteria (it stops when the relative residual error is below a given delta).
- `kSparseSNPA.m` is a modified version of SNPA where the projection step is replaced by a k-sparse projection step, performed by `arborescent`.

## Tests

`snpa_v3` contains the original code of SNPA, and associated scripts and functions. It has been written by Nicolas Gillis. A more recent version may be available [on his website](https://sites.google.com/site/nicolasgillis/code).

`test/testssnmf.m` is a simple test script I use during development to check if BRASSENS is working well. I also use it to generate nice figures.

`test/testsynth.m` generates synthetic data sets and tests BRASSENS on them.

`test/urban/testurban.m` uses BRASSENS for the unmixing of the well-known hyperspectral image Urban ([downloaded from there](http://www.escience.cn/people/feiyunZHU/Dataset_GT.html)).
