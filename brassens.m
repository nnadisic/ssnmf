function [J,H,nbcandidates,tmpJ] = brassens(M,k,delta,rmax)
% BRASSENS Relies on Assumptions of Separability and Sparsity for Elegant
% NMF Solving
%
% *** Description ***
% BRASSENS solves Sparse Separable NMF. It first uses SNPA and then a
% routine based on kSparseSNPA to solve the following problem: given a
% matrix M, find W = M(:,J) and H col-wise k-sparse such that M ~= W * H.
%
% Input:
% M is the data matrix (needs to be normalized such that each
% column sum to 1)
% k is the desired column-wise sparsity level of H (number of
% nonzero entries)
% delta is the relative-error tolerance, for the noisy case
% rmax is the maximum number of columns extracted
%
% Output:
% J the index set of the extracted columns
% H the optimal weights
%

% Extract the exterior vertices with classic SNPA
[extJ,H] = deltaSNPA(M,delta,rmax);
% Extract the candidate interior vertices with kSSNPA
[tmpJ,~] = kSparseSNPA(M,k,delta,extJ,H);
tmpJ
% Remove exterior vertices from candidates
tmpJ = setdiff(tmpJ,extJ);
nbcandidates = length(tmpJ);

% Discard all candidates j such that
% min_{h k-sparse} || M(:,j) - M(:,candJ\{j}) * h ||_2^2 > 0
J = extJ;
for i = 1 : length(tmpJ)
    % fprintf("%i nnls subproblem over %i\n", i, length(tmpJ));

    tmpW = M(:,[tmpJ extJ]);
    tmpW(:,i) = [];
    [tmpH,~,~] = arboNNLSMex(tmpW,M(:,tmpJ(i)),k);

    if norm(M(:,tmpJ(i))-(tmpW*tmpH),'fro') > delta
        J = [J tmpJ(i)];
    end
end
% Compute final H
[H,~,~,~] = arborescentMex(M,M(:,J),k);
end
