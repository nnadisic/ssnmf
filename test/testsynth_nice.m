% A script to generate synthetic data sets for SSNMF to test BRASSENS

clear all; clc;

nbiter = 1; % number of iterations for each set of parameters

%% Main loop
for r = [6]

    %% Parameters
    % For data
%     r = m + ceil(m/2);
    m = 3;
    n = 10*r;
    k = ceil(m/2);

    % For BRASSENS
    delta = 1e-5;
    rmax = r;
    targetk = k;

    % For display
    displayplot = true;

    fprintf("Parameters: m=%i n=%i r=%i k=%i\n", m, n, r, k);

    %% Loop for 1 set of parameters
    nbcand = zeros(1,nbiter); % number of candidates left after screening
    times = zeros(1,nbiter);
    for it = 1:nbiter
        fprintf("Iter %i out of %i\n", it, nbiter);

        %%%%%%%% Data generation
        % Generate W with m exterior points and r-m interior points
%         W = sample_dirichlet(ones(1,m),m);
%         Hw = sample_dirichlet(ones(1,r-m),m);
        % Generate W with m exterior points and r-m interior points
%         W = rand(m,rext);
%         Hw = rand(rext,rint);
        W = eye(m) + rand(m)/10;
        Hw = (ones(m, r-m) + (r-m)*eye(m, r-m)) / (2*(r-m));

        W = [W W*Hw];
        % Generate H = [I,H'] with H' col-wise k-sparse
%         H = zeros(r,n-r);

        % Generate very nice H for r=6; rint=3 and n=60
        H = zeros(r,54);
        % Generate on exterior segments
        for j = 1:18
            col = rand(r,1);
            col([randi(3) 4 5 6]) = 0;
            H(:,j) = col;
        end
        % Generate on segments between interior vertices
        for j = 19:36
            col = rand(r,1);
            col([(randi(3) + 3) 1 2 3]) = 0;
            H(:,j) = col;
        end
        % Generate on segment between one ext and one int
        for j = 36:54
            col = rand(r,1);
            n = randi(3);
            col(setdiff(1:6, [n n+3])) = 0;
            H(:,j) = col;
        end
%         for j = 1 : n-r
            % Generate k-sparse column
%             col = sample_dirichlet(ones(1,r),1)';
%             col(randperm(r,r-k)) = 0;
            % Copy to H
%             H(:,j) = col;
%         end
        H = [eye(r) H];
        % Normalize (all columns sum to 1)
        H = H ./ sum(H);
        W = W ./ sum(W);
        % Compute M
        M = W*H;

        %%%%%%%% Run BRASSENS
        tic;
        [myJ, myH, nbc] = brassens(M,targetk,delta,rmax);
        times(it) = toc;
        myW = M(:,myJ);
        nbcand(it) = nbc;
    end %% end loop for 1 set of params

    fprintf("Results for m=%i n=%i r=%i k=%i\n", m, n, r, k);
    fprintf("Median of number of candidates %i\n", median(nbcand));
    fprintf("Median of running time %f\n", median(times));

end %% End main loop


%% Display results
% disp("Results");
% fprintf("Median of number of candidates %i\n", median(nbcand));
% fprintf("Median of running time %f\n", median(times));
% fprintf("Selected vertices:");
% disp(myJ);

% Plot (if in 3d and displayplot defined as true)
if m == 3 && displayplot
    % Cols of M (data points)
    dpt = scatter3(M(1,:),M(2,:),M(3,:),15,'oblue');
    hold on;
    % Cols of real W that are not found in Wtoprint
    Wn = W(:,not(ismember(W',myW','rows')'));
    npt = scatter3(Wn(1,:),Wn(2,:),Wn(3,:),60,'xred','LineWidth',2);
    % Cols of computed W (selected points)
    spt = scatter3(myW(1,:),myW(2,:),myW(3,:),60, ...
                   'ogreen','filled');
    % Draw line between all pairs of points of W
    for j = 1 : r
        for jj = j+1 : r
            line([W(1,j), W(1,jj)], [W(2,j), W(2,jj)], [W(3,j), W(3,jj)]);
        end
    end
    % Draw unit simplex with dashed lines
    simplex = line([0 0],[0 1],[1 0],'LineStyle','--');
    line([0 1],[0 0],[1 0],'LineStyle','--');
    line([0 1],[1 0],[0 0],'LineStyle','--');
    % Position camera orthogonal to unit simplex
    view([45,45,45])
    hold off;
end
