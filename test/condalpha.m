function out = condalpha(A,k)
%ALPHA Compute alpha from A and k

n = size(A,2);

out = inf;
for i = 1 : n
    bi = A(:,i);
    Ai = A(:,[1:i-1,i+1:n]);
    [x,~,~] = arboNNLSMex(Ai,bi,k);
    out = min(out,norm(Ai*x-bi));
end

end

