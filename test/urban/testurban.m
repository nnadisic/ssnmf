% Test of BRASSENS for SSNMF with Urban hyperspectral image
clear all; clc;

% Parameters
delta = 0.04;
rmax = 5;
k = 2;

% Load file
load('Urban.mat')

% Select 3 bandwidths (out of 162)
K = SPA(A,3);
% Reduce the dataset to these 3 bandwidths
As = A(:,K)';
% Normalize columns to one
for i = 1 : size(As,2)
      As(:,i) = As(:,i)/sum(As(:,i));
end 

% Solve with SNPA
disp("Solve with SNPA");
tic;
[snpaJ, snpaH] = SNPA(As,rmax);
toc

% Calculer k-sparse H optimal et comparer avec ksSNPA 
% Solve with BRASSENS
disp("Solve with BRASSENS");
tic;
[myJ, myH, nbc] = brassens(As,k,delta,rmax);
toc

% Display reconstruction errors
norm(As-As(:,snpaJ)*snpaH,'fro')/norm(As,'fro')
norm(As-As(:,myJ)*myH,'fro')/norm(As,'fro')
nbc

% Compute abundances with the original matrix
A = A';
snpaHclean = nnlsHALSupdt(A,A(:,snpaJ),snpaH);
[myHclean,~,~,~] = arborescentMex(A,A(:,myJ),k,myH);

% Display SNPA abundance map
affichage(snpaHclean',6,307,307);
% Display SSNMF abundance map
affichage(myHclean',6,307,307);

% Plot data points and vertices found by SNPA and SSNMF
figure(3);
plot3(As(1,:),As(2,:),As(3,:),'bo');
hold on;
plot3(As(1,snpaJ),As(2,snpaJ),As(3,snpaJ),'rx','MarkerSize',20,'LineWidth',1.5);
plot3(As(1,myJ),As(2,myJ),As(3,myJ),'g^','MarkerSize',20,'LineWidth',1.5);
view([45,45,45]);
hold off;

