% To test BRASSENS for SSNMF
% M  =  W  *  H
% m*n   m*r   r*n and col-wise k-sparse

clear all; clc;

%% Parameters
% For data
m = 3;
n = 30;
r = 4;
k = 2;

% For SSNMF
delta = 1e-6;
rmax = 4;
targetk = 2;

% For display
dx = 0.1; % Displacement so text does not overlay data points
dy = 0.1;
dz = 0;
printcandidates = true;

%% Data generation

% Generate W
%W = rand(m,r);
W = sample_dirichlet(ones(1,r),m);
W = [1 1 14 7
     1 12 1 6
     8 2 5 6];

% Generate H = [I,H'] with H' col-wise k-sparse
H = zeros(r,n-r);
for j = 1 : n-r
    % Generate k-sparse column
    col = sample_dirichlet(ones(1,r),1)';
    col(randperm(r,r-k)) = 0;
    % Copy to H
    H(:,j) = col;
end
H = [eye(r) H];

% Normalize (all columns sum to 1)
H = H ./ sum(H);
W = W ./ sum(W);

% Compute M
M = W*H;


%% Tests

% Run SNPA
[snpaJ, snpaH] = SNPA(M,rmax,0);
snpaW = M(:,snpaJ);

% Run BRASSENS
[myJ, myH, nbcandidates, candidatesJ] = brassens(M,targetk,delta,rmax);
myW = M(:,myJ);
candidatesW = M(:,candidatesJ);


%% Display results

disp("Test results")

snpaJ
if size(snpaJ,2) == r
    norm(M-snpaW*snpaH,'fro')
end
myJ
candidatesJ
if size(myJ,2) == r
    norm(M-myW*myH,'fro')
end

%% Scatter plot in 3D

if m == 3
    % What W you want to print
    Wtoprint = myW;
    % Cols of M (data points)
    dpt = scatter3(M(1,:),M(2,:),M(3,:),15,'oblue');
    hold on;
    % Cols of real W that are not found in Wtoprint
    Wn = W(:,not(ismember(W',Wtoprint','rows')'));
    npt = scatter3(Wn(1,:),Wn(2,:),Wn(3,:),60,'xred','LineWidth',2);
    % Cols of computed W (selected points)
    spt = scatter3(Wtoprint(1,:),Wtoprint(2,:),Wtoprint(3,:),60, ...
                   'ogreen','filled');
    % Labels on selected points
    % for j = 1 : r
    %     text(W(1,j)+dx, W(2,j)+dy, W(3,j)+dz, int2str(j))
    % end
    % Draw line between all pairs of points of W
    for j = 1 : r
        for jj = j+1 : r
            line([W(1,j), W(1,jj)], [W(2,j), W(2,jj)], [W(3,j), W(3,jj)]);
        end
    end
    % Draw unit simplex with dashed lines
    simplex = line([0 0],[0 1],[1 0],'LineStyle','--');
    line([0 1],[0 0],[1 0],'LineStyle','--');
    line([0 1],[1 0],[0 0],'LineStyle','--');
    % Legend
    % legend([dpt, spt, npt, simplex], "Data points $M(:,j)$",...
    %        "Selected vertices $W(:,j)$","Vertices not identified",...
    %        "Unit simplex",'Interpreter','latex')
    if printcandidates == true
        % Candidates in Brassens
        cpt = scatter3(candidatesW(1,:),candidatesW(2,:),...
            candidatesW(3,:),60,'ored','filled');
    end
    % Position camera orthogonal to unit simplex
    view([45,45,45])
    hold off;
end