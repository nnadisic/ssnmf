function [J,H] = kSparseSNPA(M,k,delta,initJ,initH)

% This is a version of SNPA modified by changing the projection step to a
% sparse projection using arborescent, done with an exact sparse NNLS
% solver. (The original code of SNPA was written by Nicolas Gillis.)
%

[m,n] = size(M);
normM = sum(M.^2);
nM = max(normM);

% If initJ and initH are provided, use them
if nargin == 5
    i = length(initJ) + 1;
    J = initJ;
    H = initH;
    % Init residual
    R = M - M(:,J)*H;
    normM = sum(R.^2);
else
    i = 1;
end

normM1 = normM;

% While ||R||_F / ||M||_F <= delta
while true
    % Select the column of M with largest l2-norm
    [a,b] = max(normM);
    % Norms of the columns of the input matrix M
    % if i == 1, normM1 = normM; end
    % Check ties up to 1e-6 precision
    b = find((a-normM)/a <= 1e-6);
    % In case of a tie, select column with largest norm of the
    % input matrix M
    if length(b) > 1, [c,d] = max(normM1(b)); b = b(d); end
    % Update the index set, and extracted column
    J(i) = b; U(:,i) = M(:,b);

    % Update residual
    if i == 1
        % Compute H with arborescent, no initialization
        [H,~,~,~] = arborescentMex(M,M(:,J),k);
    else
        % Compute H with arborescent, init with previous H
        H(:,J(i)) = 0;
        h = zeros(1,n); h(J(i)) = 1;
        H = [H; h];
        [H,~,~,~] = arborescentMex(M,M(:,J),k,H);
    end
    R = M - M(:,J)*H;

    % Update norms
    normM = sum(R.^2);

    i = i + 1;

    % Display residual's norm at every iteration
    % norm(R,'fro')/norm(M,'fro')

    if norm(R,'fro')/norm(M,'fro') <= delta
        break;
    end
end

end % of function kSparseSNPA