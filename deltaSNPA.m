function [J,H] = deltaSNPA(M,delta,rmax) 

% TODO descr

[m,n] = size(M); 
maxitn = 100; 

normM = sum(M.^2); 
R = inf;

i = 1;
while i <= rmax && norm(R,'fro')/norm(M,'fro') > delta
    % Select the column of M with largest l2-norm
    [a,b] = max(normM); 
    % Norms of the columns of the input matrix M 
    if i == 1, normM1 = normM; end 
    % Check ties up to 1e-6 precision
    b = find((a-normM)/a <= 1e-6); 
    % In case of a tie, select column with largest norm of the input matrix M 
    if length(b) > 1, [c,d] = max(normM1(b)); b = b(d); end
    % Update the index set, and extracted column
    J(i) = b; U(:,i) = M(:,b); 
    
    % Update residual 
    if i == 1
        % Initialization using 10 iterations of coordinate descent
        H = nnlsHALSupdt(M,M(:,J),[],10); 
        % Fast gradient method for min_{y in Delta} ||M(:,i)-M(:,J)y||
        H = FGMfcnls(M,M(:,J),[],maxitn); 
    else
        H(:,J(i)) = 0; 
        h = zeros(1,n); h(J(i)) = 1; 
        H = [H; h]; 
        H = FGMfcnls(M,M(:,J),H,maxitn); 
    end
    R = M - M(:,J)*H; 
    
    % Update norms
    normM = sum(R.^2); 
   
    i = i + 1; 
end

end % of function FastSepNMF